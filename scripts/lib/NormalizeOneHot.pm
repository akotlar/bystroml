use 5.10.0;
use strict;
use warnings;

package NormalizeOneHot;
use Mouse;
use lib './lib';
use IO;
use DDP;
use Scalar::Util qw/looks_like_number/;
use List::Util qw/uniq/;

has in => (is => 'ro', isa => 'Str', required => 1);
has out => (is => 'rw', isa => 'Maybe[Str]', required => 1);

sub go {
  my $self = shift;
  

  
  # my $outFh = $io->getWriteFh($self->out);

  # Reset 

  # 1) Splits each transcript to a separate line
  ### a variant can be considered to affect multiple transcripts
  ### or equivalently as multiple variants (since potentially diff effects)
  $self->dropColumnsRenameMnpSplitIndels();

  # $self->getHeader();

  
}

# MNPs are contiguous SNPs; very few and not having "MNP" label
# means little
# Similarly, Indels are just runs of deletions
# We provide information about every change that occured as part of the deletion
# in order to retain the structure of its impact
# However, its impact is most closely tied to the % 3; ie if evenly divided by 3
# and it is unlikely that any two people with deletions in a particular area
# especially rare deletions, will have the exact same deletion
# By splitting the deletion into N parts, we can treat the each deleted base
# as another sampling of the same spot
# Handle insertions similarly
# Gives us ability to over-sample in a genomically relevant way
# Also, some features are calculated on a per-position basis (CADD, PhyloP, PhastCons)
sub dropColumnsRenameMnpSplitIndels {
  my $self = shift;

  my $io = IO->new();
  my $inFh = $io->getReadFh($self->in);

  my $header = <$inFh>;

  $io->setLineEndings($header);

  chomp $header;

  my $out = $self->out;

  my $outFh = $io->getWriteFh($out);

  my %wantedFields;

  my $skip = {
    'discordant' => 1,
    'homozygotes' => 1,
    'heterozygotes' => 1,
    'missingGenos' => 1,
    'heterozygosity' => 1,
    'homozygosity' => 1,
    'missingness' => 1,
    'sampleMaf' => 1,
    'alleleIdx' => 1,
    'refSeq.nearest.name2' => 1,
    'refSeq.nearest.name' => 1,
    'refSeq.description' => 1,
    'refSeq.rfamAcc' => 1,
    'refSeq.kgID' => 1,
    'refSeq.codonPosition' => 1,
    'refSeq.name2' => 1,
    # 'refSeq.name' => 1,
    'refSeq.refCodon' => 1,
    'refSeq.altCodon' => 1,
    'refSeq.mRNA' => 1,
    'refSeq.spID' => 1,
    'refSeq.spDisplayID' => 1,
    'refSeq.protAcc' => 1,
    'dbSNP.strand' => 1,
    'dbSNP.observed' => 1,
    'dbSNP.class' => 1,
    'dbSNP.func' => 1,
    'dbSNP.alleles' => 1,
    'dbSNP.name' => 1,
    'dbSNP.alleleNs' => 1,
   # 'dbSNP.alleleFreqs' => 1,
    # 'dbnsfp.ExAC_cnv_flag' => 1,
    'gnomad.genomes.id' => 1,
    'gnomad.exomes.id' => 1,
    'gnomad.genomes.alt' => 1,
    'gnomad.exomes.alt' => 1,
    #we'll split trTv into 2 classes: transition and transversion
    'trTv' => 1,
  };

  my $i = -1;
  my $count= 0;
  # For refseq, we'll replace with the count of genes, rather than
  # the names themselves
  # Also, the chr/position determines the genes; what's more important than the name

  # For dbSNP, with a simple boolean (seen or not)
  my $dbSnpIdx;
  my $pathogenicIdx;
  my $altAminoIdx;
  my $refAminoIdx;
  my $gnomadGenomesIdIdx;
  my $gnomadExomesIdIdx;
  my $refSeqExonicIdx;
  my @gnomadExomesAfIdxOverall;
  my @gnomadGenomesAfIdxOverall;
  my @gnomadExomesAfIdxOurs;
  my $dbSnpAfIdx;
  my $dbSnpAllelesIdx;
  my $trTvIdx;
  my @headerFields;
  my $cnvFlagIdx;
  my $expressionIdx;
  my $caddIdx;
  my @refSeqFieldIndices;
  my $refSeqSiteTypeIdx;
  my @dbnsfpCommaSplit;
  my $traitIdx;
  my $strandIdx;
  my $typeIdx;
  my @fields =  (split '\t', $header);
  my $skipped = 0;
  # exit;
  for my $field (split '\t', $header) {
    $i++;

    if($field =~ /dbSNP.name/) {
      $dbSnpIdx = $i;
    }

    if($field =~ /gnomad.genomes.id/) {
      $gnomadGenomesIdIdx = $i;
    }

    if($field =~ /gnomad.exomes.af/) {
      push @gnomadExomesAfIdxOverall, $i;
    }

    if($field =~ /gnomad.genomes.af/) {
      push @gnomadGenomesAfIdxOverall, $i;
    }

    if($field =~ /trTv/) {
      $trTvIdx = $i;
    }

    # We'll skip all dbSNP data, except to state that dbSNP has a record here
    # and mention how 
    # We skipt gnomad AN because that information isn't really terribly useful
    # essentially it's a function of the study, +/- quality issues
    # there will be very little separation
    # between classes based on that feature
    # We will combine gnomad.exomes together with gnomad.genomes
    # Essentially gnomad exomes has more samples/covered base, but gnomad genomes 
    # has more coverage overall
    if($field !~ /refSeq\.clinver/ && $field !~ /clinvar/ && !defined $skip->{$field} &&
      $field !~ /gnomad.exomes.an/ && $field !~ /gnomad.genomes/) {
      $wantedFields{$i} = $field;
      push @headerFields, $field;
      $count++;

      next;
    }
  }

  # Last column is whether essential amino acid was disrupted
  # may help us drop the altAminoAcid and refAminoAcid columns
  push @headerFields, "transition", "transversion", "essentialDisruption", "conditionallyEssentialDisruption";

  my $essentialIdx = $#headerFields - 1;
  my $conditEssentialIdx = $#headerFields;

  p @headerFields;
  # exit;
  $i = 0;
  for my $field (@headerFields) {
    if($field =~ /gnomad.exomes.af/) {
      push @gnomadExomesAfIdxOurs, $i;
    }

    if($field =~ /dbSNP.alleleFreqs/) {
      $dbSnpAllelesIdx = $i;
    }

    if($field =~ /refSeq.siteType/) {
      $refSeqSiteTypeIdx = $i;
    }

    if($field =~ /refSeq.exonicAlleleFunction/) {
      $refSeqExonicIdx = $i;
    }

    if($field =~ /pathogenic/i) {
      $pathogenicIdx = $i;
    }

    if($field =~ /refSeq.altAminoAcid/) {
      $altAminoIdx = $i;
    }

    if($field =~ /dbnsfp.ExAC_cnv_flag/) {
      $cnvFlagIdx = $i;
    }

    if($field =~ /refSeq.refAminoAcid/) {
      $refAminoIdx = $i;
    }

    if($field =~ /cadd/) {
      $caddIdx = $i;
    }

    if($field =~ /refSeq/ || $field =~ /dbnsfp/ && $field !~ /refSeq.nearest/) {
      push @refSeqFieldIndices, $i;
    }

    if($field =~ /dbnsfp.Expression/) {
      $expressionIdx = $i;
    }

    if($field =~ /dbnsfp.Trait_association\(GWAS\)/) {
      $traitIdx = $i;
    }

    if($field =~ /refSeq.strand/) {
      $strandIdx = $i;
    }

    $i++;
  }

  say "strand idx is $strandIdx";

  $header = join("\t", @headerFields);
  $header =~ s/gnomad.exomes/gnomad/g;
  p $header;
  say $outFh $header;

  # my $refNameIdx;
  while(<$inFh>) {
    chomp;

    $_ =~ s/\!/NA/g;

    my @fields;

    my @allFields = split '\t', $_;

    $allFields[0] =~ s/chr//;

    if($allFields[0] eq 'X') {
      $allFields[0] = 23;
    } elsif($allFields[0] eq 'Y') {
      $allFields[0] = 24;
    } elsif($allFields[0] eq 'M') {
      $allFields[0] = 25;
    }

    my $idx = 0;
    for my $field (@allFields) {
      if($wantedFields{$idx}) {
        push @fields, $field;
      }
      $idx++;
    }

    # Prevent its accidental use later on
    undef $idx;

    if($allFields[$trTvIdx] == 1) {
      push @fields, 1, 0;
    } elsif($allFields[$trTvIdx] == 2) {
      push @fields, 0, 1;
    } elsif($allFields[$trTvIdx] == 0) {
      push @fields, 0, 0;
    }

    if($fields[2] eq 'MNP') {
      $fields[2] = 'SNP';
    }

    my $lengthOfVar = 1;

    # say "deletion is $fields[3]";

    if($fields[2] eq "DEL" || $fields[2] eq "INS") {
      $lengthOfVar = $fields[2] eq "INS" ? 2 : -$fields[3];
      $fields[3] = $fields[2] eq "DEL" ? "-" : "+";
    }

    my @rows;
    $#rows = $lengthOfVar - 1;

    for my $field (@fields) {
      # p $field;
      $field = [split /\|/, $field];
    }

    # say "row is length " . (@rows);

    for my $i (0 .. $#rows) {
      $rows[$i] //= [];

      # p @fields;
      my $fidx = 0;
      for my $field (@fields) {
        $rows[$i][$fidx] = @$field > 1 ? $field->[$i] : $field->[0];

        $rows[$i][$fidx] //= 'NA';

        $fidx++;
      }

      ;
      # Add the position offset to the deletion
      # For insertions, we consider the insertion to disrupt the first base,
      # or the next one downstream
      $rows[$i][1] += $i;
    }
    
    #   say "rows";
    # p @rows;

    for my $outerRow (@rows) {
      my @innerRows;

      my %refData;

      for my $idx (@refSeqFieldIndices) {
        my @data = split /[;]/, $outerRow->[$idx];
       
        $refData{$idx} = \@data;
      }

      # This is guaranteed to have 1 entry per transcript,
      # Cannot remmeber if I enforce that for exonicAlleleFunction
      # my $numRefData = split /[;]/, $outerRow->[$refSeqSiteTypeIdx];
      # sa$numRefData;
      my $numRefData = @{$refData{$refSeqSiteTypeIdx}};

      # next;
      for my $i (0 .. $numRefData - 1) {
        my @row = @$outerRow;

        if($row[$refSeqSiteTypeIdx] eq 'intergenic') {
          $skipped++;
          next;
        }

        for my $idx (@refSeqFieldIndices) {
          $row[$idx] = $refData{$idx}->[$i] || "NA";
        }

        # for my $val (split /[;]/, $allFields[$dbSnpIdx]) {
        #   if($val ne '!') {
        #     $row[$dbSnpIdx] = 1;
        #   } else {
        #     $row[$dbSnpIdx] = 0;
        #   }
        # }

        $row[$pathogenicIdx] = $row[$pathogenicIdx] eq 'pathogenic' ? 1 : 0; 

        # p @gnomadExomesAfIdx;
        # If gnomad exomes doesn't have our allele, it is most likely really missing
        # but only if we're within a gene
        # the exomes database has no coverage outside of genes
        my @gnomadGenomesValues = map { $allFields[$_] } @gnomadGenomesAfIdxOverall;

        my $i = -1;

        for my $eAfIdx (@gnomadExomesAfIdxOurs) {
          $i++;

          if($row[$eAfIdx] eq 'NA') {
            my $genomesAf = $gnomadGenomesValues[$i];
            # p $genomesAf;

            if($genomesAf ne 'NA') {
              $row[$eAfIdx] = $genomesAf;
            } else {
              # say "Since not in exomes or genomes, setting $eAfIdx to 0";
              $row[$eAfIdx] = 0;
              # say "missing exomes $eAfIdx";
              # p @row;
            }
          }


        }
        # if($allFields[$gnomadExomesIdIdx] eq 'NA') {
        #   if($row[$refSeqIdx] !~ /intergenic/) {
        #     for my $idx (@gnomadExomesAfIdx) {
        #       $row[$idx] = 0;
        #     }
        #   }
        # }

        # # If gnomad doesn't have our allele, it is most likely really missing
        # if($allFields[$gnomadGenomesIdIdx] eq 'NA') {
        #   for my $idx (@gnomadGenomesAfIdx) {
        #     $row[$idx] = 0;
        #   }
        # }

        if($row[$refSeqExonicIdx] ne 'NA') {
          if($row[$refSeqExonicIdx] =~ /frameshift|stopGain|stopLoss/ || $row[$refSeqSiteTypeIdx] =~ /spliceDonor|spliceAcceptor/) {
            $row[$refSeqExonicIdx] = "lof";
          }
        } else {
          $row[$refSeqExonicIdx] = $row[$refSeqSiteTypeIdx];

          # say "set to site type";
          # p @row;
        }

        if($row[$cnvFlagIdx] eq 'N') {
          $row[$cnvFlagIdx] = 0;
        } else {
          $row[$cnvFlagIdx] = 1;
        }


        if($row[$dbSnpAllelesIdx] eq 'NA') {
          $row[$dbSnpAllelesIdx] = 0;
        } else {
          my @afs = sort { $a <=> $b } map { $_ eq 'NA' ? () : $_ } split /[;]/, $row[$dbSnpAllelesIdx];
          my $target;

          if(@afs == 0) {
            $target = 0;
          } elsif(@afs == 1 && index($row[$dbSnpAllelesIdx], ';') > -1) {
            # We have something like NA ; 1
            # The 1 is the reference
            # The NA suggests so rare essentially 0 frequency in population
            $target = 0;
          } elsif(@afs >= 2) {
            $target = $afs[-2];
          }

          # This is just crap from dbSNP.
          # Again means we only see the reference allele, 
          # although we have a record somehow for an alternate allele
          # Example:
          # rs68118007|rs68118007;rs568682203;rs867901344 -|-;+;+ -;GT|-;GT;C;T;-;A;ACA deletion|deletion;single;insertion  untranslated-3|untranslated-3;untranslated-3;untranslated-3 -|-;C;T;NA  2|2;4987;21;NA  1|1;0.995807;0.004193;NA
          if(!defined $target && $row[$dbSnpAllelesIdx] == 1) {
            $target = 0;
          }
          # It is important to know whether variants are frequently seen here
          # this field also includes the reference allele frequency, so skip that
          # the next largest allele frequency will be the most common allele seen
          # If nothing is seen, a reasonable value is 0, since dbSNP covers genome wide,
          # albeit unequally
          $row[$dbSnpAllelesIdx] = $target;
        }

        # if($row[$refSeqExonicIdx] =~ /indel-frameshift/) {
        #   # There is no amino acid if insertion or deletion
        #   # With insertino its a bit muddier, but since we're not re-translating
        #   # the protein, it is easier to just say that the amino acid is missing
        #   $row[$altAminoIdx] = "disrupted";
        # }

        # if($row[$refSeqExonicIdx] =~ /indel-nonFrameshift/) {
        #   # When it's a
        #   $row[$altAminoIdx] = $row[$refAminoIdx];
        # }


        # If it's not synonymous or NA, it must be lof or nonsynoymous
        if($row[$refSeqExonicIdx] eq 'lof' || $row[$refSeqExonicIdx] eq 'nonSynonymous') {
          if($row[$refAminoIdx] =~ /F|V|T|W|M|L|I|K|H|V/) {
            $row[$essentialIdx] = 1;
          } else {
            $row[$essentialIdx] = 0;
          }

          if($row[$refAminoIdx] =~ /R|C|G|Q|P|Y/) {
            $row[$conditEssentialIdx] = 1;
          } else {
            $row[$conditEssentialIdx] = 0;
          }

          # say "setting as disruptive";
          # p @row;
        } else {
          # If we're synonymous, or say in an intronic site, UTR5, UTR3 by definition
          # we're not disrupting an essential amino acid
          $row[$essentialIdx] = 0;
          $row[$conditEssentialIdx] = 0;

          # say "setting as not";
          # p @row;
        }

        # If in deletion/insertion we will hav many cadd values
        # In essence the worst cadd value is probably closer to reality
        # since indels are more disruptive on average than SNPs
        my @cadd = split /[;]/, $row[$caddIdx];

        if(@cadd > 1) {
          @cadd = sort { $a <=> $b } @cadd;
          $row[$caddIdx] = $cadd[-1];
        }
        # if(!$row[16]) {
        #   p $_;
        # }
        # p $fields[$expressionIdx];
        my @expressionVals = map { $_ ne 'NA' } split /[,]/, $row[$expressionIdx];

        $row[$expressionIdx] = scalar @expressionVals;

        # may have left the brackets in there
        $row[$traitIdx] =~ s/[\[\]]*//g;

        # Count the number of unique diseases associated with this mutation
        # rather than using the disease categories themselves
        if($row[$traitIdx] ne 'NA') {
          my @vals = uniq(split ',', $row[$traitIdx]);
          $row[$traitIdx] = scalar @vals;
        } else {
          $row[$traitIdx] = 0;
        }

        if($row[$strandIdx] ne 'NA') {
          $row[$strandIdx] = $row[$strandIdx] eq '+' ? 1 : 0;
        }

        # won't have an amino acid in utr, splice, intronic sites
        # so after one hot encode
        if($row[$refAminoIdx] eq 'NA') {
          $row[$refAminoIdx] = 'none';
        }

        if($row[$altAminoIdx] eq 'NA') {
          $row[$refAminoIdx] = 'none';
        }

        # say "my expressionCount is $expressionCount";
        # Summarize the expression classes
        # This is essntially a category that can contain hundreds of values
        # Initially, lets try to count the number of tissues this gene is exprssed in
        # and use that as a proxy to importance of function
        # Since we are unlikely to have many observations per feature
        # when this feature is split into 10's - hundreds of features
        # $fields[$expressionIdx] = $expressionCount;
        say $outFh join("\t", map { $_ eq 'NA' ? '' : $_ } @row);
      }
    }
  }

  say STDERR "skipped $skipped";
  $self->out($out);
}

# sub splitRefSeq {
#   my $self = shift;

#   my $io = IO->new();
#   my $inFh = $io->getReadFh($self->in);

#   my $header = <$inFh>;

#   $io->setLineEndings($header);

#   chomp $header;

#   my $out = $self->out . ".split.tsv";

#   my $outFh = $io->getWriteFh($out);

#   say $outFh $header;

#   my @wantedFields;

#   for my $field (split '\t', $header) {
#     if($field =~ /exonicAlleleFunction)
#   }

#   while(<$inFh>) {
#     chomp;


#   }
# }


sub getHeader {
  my ($self) = @_;

  my $io = IO->new();
  my $inFh = $io->getReadFh($self->in);

  my $header = <$inFh>;

  $io->setLineEndings($header);

  chomp $header;

  # Clinvar clinical significance is really about the diseases
  # We mostly 
  my %thingsToSkip = (
    discordant => 1,
    homozygotes => 1, heterozygotes => 1, missingGenos => 1,
    heterozygosity => 1, homozygosity => 1, missingness => 1,
    sampleMaf => 1, "clinvar.clinicalSignificance" => 1, 
  );

  my $clinvarIdx;

  my %levels;

  # Some categorical data is really ordinal
  my %ordinalNoOneHot = (

  );
  my @header = map { $_ } split '\t', $header;

  # skip some stuff;
  # my $field (@header) {

  # }

  # p @header;

  # my %unique;

  # reset
  $io->setLineEndings("\n");
}

# sub normalize {
#   my ($self, $header, $headerCatMap) = @_;

#   my $header = <$inFh>;

#   $self->setLineEndings($header);


# }
__PACKAGE__->meta->make_immutable();
1;
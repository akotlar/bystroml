use 5.10.0;
use strict;
use warnings;

package VerifyNoOverlap;
use Mouse;
use lib './lib';
use IO;
use DDP;
use Scalar::Util qw/looks_like_number/;
use List::Util qw/uniq/;
use Digest::MD5 qw/md5/;
has inTrain => (is => 'ro', isa => 'Str', required => 1);
has inTest => (is => 'rw', isa => 'Maybe[Str]', required => 1);
has removeFromTest => (is => 'ro', isa => 'Bool', default => 0);

sub go {
  my $self = shift;

  my $io = IO->new();
  my $inTrainFh = $io->getReadFh($self->inTrain);
  my $inTestFh = $io->getReadFh($self->inTest);
  my $header = <$inTestFh>;

  $io->setLineEndings($header);

  chomp $header;

  my $chromIdx;
  my $posIdx;
  
  my $i = -1;
  for my $field (split '\t', $header) {
    $i++;

    if($field =~ /chrom/) {
      $chromIdx = $i;
    }

    if($field =~ /pos/) {
      $posIdx = $i;
    }
  }

  my %chrPos;
  while(<$inTestFh>) {
    chomp;

    $chrPos{md5($_)} = 1;
  }

  $header = <$inTrainFh>;
  my $idx;
  my $overlap = 0;
  while(<$inTrainFh>) {
    chomp;

    my $id = md5($_);

    if($chrPos{$id}) {
      $overlap++;
      # say $_;
    }
  }

  say STDERR "Found $overlap overlapping rows";
}

# sub splitRefSeq {
#   my $self = shift;

#   my $io = IO->new();
#   my $inFh = $io->getReadFh($self->in);

#   my $header = <$inFh>;

#   $io->setLineEndings($header);

#   chomp $header;

#   my $out = $self->out . ".split.tsv";

#   my $outFh = $io->getWriteFh($out);

#   say $outFh $header;

#   my @wantedFields;

#   for my $field (split '\t', $header) {
#     if($field =~ /exonicAlleleFunction)
#   }

#   while(<$inFh>) {
#     chomp;


#   }
# }


sub getHeader {
  my ($self) = @_;

  my $io = IO->new();
  my $inFh = $io->getReadFh($self->in);

  my $header = <$inFh>;

  $io->setLineEndings($header);

  chomp $header;

  # Clinvar clinical significance is really about the diseases
  # We mostly 
  my %thingsToSkip = (
    discordant => 1,
    homozygotes => 1, heterozygotes => 1, missingGenos => 1,
    heterozygosity => 1, homozygosity => 1, missingness => 1,
    sampleMaf => 1, "clinvar.clinicalSignificance" => 1, 
  );

  my $clinvarIdx;

  my %levels;

  # Some categorical data is really ordinal
  my %ordinalNoOneHot = (

  );
  my @header = map { $_ } split '\t', $header;

  # skip some stuff;
  # my $field (@header) {

  # }

  # p @header;

  # my %unique;

  # reset
  $io->setLineEndings("\n");
}

# sub normalize {
#   my ($self, $header, $headerCatMap) = @_;

#   my $header = <$inFh>;

#   $self->setLineEndings($header);


# }
__PACKAGE__->meta->make_immutable();
1;
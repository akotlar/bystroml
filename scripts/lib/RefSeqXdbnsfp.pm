use 5.14.0;
use strict;
use warnings;

use lib '../';

# Adds dbnsfp to refGene
package RefSeqXdbnsfp;

our $VERSION = '0.001';

use Mouse 2;
use namespace::autoclean;
use Path::Tiny qw/path/;
use IO;
use List::Util qw/uniq/;
use DDP;

# ########## Arguments accepted ##############
# Expects tab delimited file; not allowing to be set because it probably won't ever be anything
# other than tab, and because split('\'t') is faster
# has delimiter => (is => 'ro', lazy => 1, default => "\t");
has geneFile => (is => 'ro', isa => 'Str', required => 1);
has in => (is => 'ro', isa => 'Str', required => 1);
has out => (is => 'ro', isa => 'Str');

has wantedCols => (is => 'ro', isa => 'ArrayRef', default => sub {
  return ['ExAC_cnv_flag', 'GDI-Phred', 'ExAC_pLI', 'ExAC_pRec', 'ExAC_pNull',
  'ExAC_cnv.score', 'RVIS_percentile_ExAC', 'Expression(egenetics)', 'Trait_association(GWAS)']
});

# TODO: error check opening of file handles, write tests
sub go {
  my $self = shift;

  say 'Beginning RefSeqXdbnsfp';
  my %skippedBecauseExists;

  my $io = IO->new();

  my $dbnsfpFh = $io->getReadFh($self->geneFile);

  my $header = <$dbnsfpFh>;

  #appropriate chomp
  $io->setLineEndings($header);
  chomp $header;

  my @dbNSFPheaderFields = split '\t', $header;

  # Unfortunately, dbnsfp has many errors, for instance, NM_207007 being associated
  # with CCL4L1 (in neither hg19 or 38 is this true: SELECT * FROM refGene LEFT JOIN kgXref ON refGene.name = kgXref.refseq LEFT JOIN knownToEnsembl ON kgXref.kgID = knownToEnsembl.name WHERE refGene.name='NM_207007' ;)
  # So wel'll get odd duplicates;
  # A safer option is to lose transcript specificity, but use the unique list of genes
  my @geneNameCols = qw/Gene_name/;
  my @geneNameIdx;

  my %wantedCols = map { $_ => 1 } @{$self->wantedCols};

  for my $col (@geneNameCols) {
    my $idx = 0;
    for my $dCol (@dbNSFPheaderFields) {
      if($dCol eq $col) {
        push @geneNameIdx, $idx;
      }

      $idx++;
    }
  }

  my $headerLength = @dbNSFPheaderFields;

  my @wantedIdx;
  my $idx = 0;
  for my $field (@dbNSFPheaderFields) {
    if($wantedCols{$field}) {
      push @wantedIdx, $idx;
    }

    $idx++;
  }

  # namespace
  @dbNSFPheaderFields = map { 'dbnsfp.' . $_ } map { $wantedCols{$_} ? $_ : () } @dbNSFPheaderFields;

  p @dbNSFPheaderFields;
  p @wantedIdx;
  my %dbNSFP;
  while(<$dbnsfpFh>) {
    #appropriate chomp based on line endings
    chomp;


    # $_ =~ s/\.\s*$//g;

    # Weirdly many dbnsfp fields have trailing whitespace
    my @inFields = split '\t', $_;

    if(@inFields != $headerLength) {
      die "Line length doesn't match header : $_;"
    }

    my @outFields;
    for my $idx (@wantedIdx) {
      my $field = $inFields[$idx];


      # Strip redundant words
      $field =~ s/TISSUE SPECIFICITY:\s*|FUNCTION:\s*|DISEASE:\s*//g;

      # split on [;] more effective, will split in cases like ); which /;/ won't
      my @unique = uniq(split /[;]/, $field);

      my @out;
      for my $f (@unique) {
        $f =~ s/^\s+//;
        $f =~ s/\s+$//;
        $f =~ s/\.$//;
        $f =~ s/\;$//;
        $f =~ s/\,/ /g;

        if(my @matches = $f =~ m/\[(\d+)\]{0,1}/g) {
          $f = join(',', @matches);
        }

        # if(my @matches = $f =~ m/\[\d+,\d+\]{0,1}/g) {
        #   say "match";
        #   p @matches;
        #   # $f = join(',', @matches);
        # }

        if(defined $f && $f ne '') {
          push @out, $f;
        }
      }

      $field = @out ? join ",", @out : "!";

      push @outFields, $field;
    }

    my $i = -1;
    for my $idx (@geneNameIdx) {
      $i++;

      my @vals = split ';', $inFields[$idx];

      # sometimes dbNSFP gives duplicate values in the same string...
      my %seenThisLoop;
      for my $val (@vals) {
        if($val eq '.' || $val !~ /^\w+/) {
          $self->log('fatal', "WTF: missing gene?");
        }

        $seenThisLoop{$val} = 1;

        if(exists $dbNSFP{$val}) {
          $self->log('fatal', "Duplicate entry found: $val, skipping : $_");
          next;
        }

        $dbNSFP{$val} = \@outFields;
      }
    }
  }

  # Need to reset line endings here, or getReadFh may not operate correctly
  $io->setLineEndings("\n");

  my $fh = $io->getReadFh($self->in);
  my $outFh; 

  my $file = $self->in;

  $file =~ s/.gz$//;
  my $outFile = $self->out ? $self->out : $file . '.with_dbnsfp.gz';

  $outFh = $io->getWriteFh($outFile);

  $header = <$fh>;

  $io->setLineEndings($header);

  chomp $header;

  say $outFh join("\t", $header, @dbNSFPheaderFields);

  my $geneIdx;
  my $i = 0;
  for my $field (split '\t', $header) {
    if($field eq 'refSeq.name2') {
      $geneIdx = $i;
    }

    $i++;
  }

  while(<$fh>) {
    chomp;

    my @fields = split '\t', $_;

    my $foundDbNFSP;
    my @out;
    for my $indel (split /\|/, $fields[$geneIdx]) {
      my @geneData;
      for my $gene (split /[;]/, $indel) {
        # Empirically determine
        if($dbNSFP{$gene}) {
          if(@geneData) {
            for (my $i = 0; $i < @geneData; $i++) {
              if($dbNSFP{$gene}->[$i]) {
                $geneData[$i] .= ';' . $dbNSFP{$gene}->[$i];
              }
            }

            next;
          }

          @geneData = @{$dbNSFP{$gene}};
        }
      }

      if(!@out) {
        @out = @geneData;
        next;
      }

      for (my $i = 0; $i < @geneData; $i++) {
        $out[$i] .= '|' . $geneData[$i];
      }
    }

    # p @out;

    say $outFh join("\t", @fields, @out ? @out : map { '!' } @dbNSFPheaderFields);
  }

  say 'Finished RefSeqXdbnsfp';
}

__PACKAGE__->meta->make_immutable;
1;
use 5.10.0;
use strict;
use warnings;

package RemoveDuplicates;
use Mouse;
use lib './lib';
use IO;
use DDP;
use Scalar::Util qw/looks_like_number/;
use List::Util qw/uniq/;
use Digest::MD5 qw/md5/;

has in => (is => 'ro', isa => 'Str', required => 1);
has out => (is => 'ro', isa => 'Str', required => 1);

sub go {
  my $self = shift;

  my $io = IO->new();
  my $inFh = $io->getReadFh($self->in);
  my $outFh = $io->getWriteFh($self->out);
  
  my $header = <$inFh>;

  $io->setLineEndings($header);

  chomp $header;

  say $outFh $header;

  my %chrPos;
  my %skip;
  my $duplicates = 0;
  while(<$inFh>) {
    chomp;

    my $id = md5($_);

    if($chrPos{$id}) {
      $duplicates++;
      $skip{$id} = 1;
    }

    $chrPos{$id} = 1;
  }

  close $inFh;

  $inFh = $io->getReadFh($self->in);
  $header = <$inFh>;

  while(<$inFh>) {
    chomp;

    my $id = md5($_);

    if(!$skip{$id}) {
      say $outFh $_;
    }
  }

  say STDERR "Skipped $duplicates";
}

__PACKAGE__->meta->make_immutable();
1;
use 5.10.0;
use strict;
use warnings;

package IO;

use Mouse 2;

use File::Which qw/which/;
use Path::Tiny qw/path/;

state $tar = which('tar');
state $gzip = which('pigz') || which('gzip');
# state $gzip = which('gzip');
state $tarCompressed = "$tar --use-compress-program=$gzip";

has gzip => (is => 'ro', isa => 'Str', init_arg => undef, lazy => 1, default => sub {$gzip});

#@param {Path::Tiny} $file : the Path::Tiny object representing a single input file
#@param {Str} $innerFile : if passed a tarball, we will want to stream a single file within
#@return file handle
# TODO: return error, don't die
sub getReadFh {
  my ( $self, $file, $innerFile) = @_;
  my $fh;
  
  if(ref $file ne 'Path::Tiny' ) {
    $file = path($file)->absolute;
  }

  my $filePath = $file->stringify;

  if (!$file->is_file) {
    die("$filePath does not exist for reading");
    die;
  }

  my $compressed = 0;
  my $err;
  if($innerFile) {
    $compressed = $innerFile =~ /\.gz$/ || $innerFile =~ /\.zip$/;

    my $innerCommand = $compressed ? "\"$innerFile\" | $gzip -d -c -" : "\"$innerFile\"";
    # We do this because we have not built in error handling from opening streams

    my $command;
    my $outerCompressed;
    if($filePath =~ /\.tar.gz$/) {
      $outerCompressed = 1;
      $command = "$tarCompressed -O -xf \"$filePath\" $innerCommand";
    } elsif($filePath =~ /\.tar$/) {
      $command = "$tar -O -xf \"$filePath\" $innerCommand";
    } else {
      die("When inner file provided, must provde a parent file.tar or file.tar.gz");
      die;
    }

    open ($fh, '-|', $command) or die("Failed to open $filePath ($innerFile) due to $!");

    # From a size standpoint a tarball and a tar file whose inner annotation is compressed are similar
    # since the annotation dominate
    $compressed = $compressed || $outerCompressed;
    # If an innerFile is passed, we assume that $file is a path to a tarball
  } elsif($filePath =~ /\.gz$/) {
    $compressed = 1;
    #PerlIO::gzip doesn't seem to play nicely with MCE, reads random number of lines
    #and then exits, so use gunzip, standard on linux, and faster
    open ($fh, '-|', "$gzip -d -c \"$filePath\"") or die("Failed to open $filePath due to $!");
  } elsif($filePath =~ /\.zip$/) {
    $compressed = 1;
    #PerlIO::gzip doesn't seem to play nicely with MCE, reads random number of lines
    #and then exits, so use gunzip, standard on linux, and faster
    open ($fh, '-|', "$gzip -d -c \"$filePath\"") or die("Failed to open $filePath due to $!");
  } else {
    open ($fh, '-|', "cat \"$filePath\"") or die("Failed to open $filePath due to $!");
  };

  # TODO: return errors, rather than dying
  return ($err, $compressed, $fh);
}

# TODO: return error if failed
sub getWriteFh {
  my ( $self, $file, $compress ) = @_;

  die("get_fh() expected a filename") unless $file;

  my $fh;
  if ( $compress || $file =~ /\.gz$/ ) {
    # open($fh, ">:gzip", $file) or die die("Couldn't open $file for writing: $!");
    open($fh, "|-", "$gzip -c > $file") or die("Couldn't open gzip $file for writing");
  } elsif ( $file =~ /\.zip$/ ) {
    open($fh, "|-", "$gzip -c > $file") or die("Couldn't open gzip $file for writing");
    # open($fh, ">:gzip(none)", $file) or die die("Couldn't open $file for writing: $!");
  } else {
    open($fh, ">", $file) or die("Couldn't open $file for writing: $!");
  }

  return $fh;
}

sub setLineEndings {
  my ($self, $firstLine) = @_;

  if($firstLine =~ /\r\n$/) {
    $/ = "\r\n";
  } elsif($firstLine =~ /\n$/) {
    $/ = "\n";
  } elsif($firstLine =~ /\015/) {
    # Match ^M (MacOS style line endings, which Excel outputs on Macs)
    $/ = "\015";
  } else {
    return "Cannot discern line endings: Not Mac, Unix, or Windows style";
  }

  return "";
}

__PACKAGE__->meta->make_immutable();
1;
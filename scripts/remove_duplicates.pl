use 5.10.0;
use strict;
use warnings;
use lib './lib';

use Getopt::Long;
use RemoveDuplicates;

my ($in, $out);

GetOptions(
  "in=s" => \$in,
  "out=s" => \$out,
);

if(!$in && !$out) {
  die "--in --out";
}

my $oneHot = RemoveDuplicates->new({
  in => $in, out => $out
});

$oneHot->go();
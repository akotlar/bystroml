use 5.10.0;
use strict;
use warnings;
use lib './lib';

use Getopt::Long;
use VerifyNoOverlap;

my ($inTest, $inTrain);

GetOptions(
  "inTest=s" => \$inTest,
  "inTrain=s" => \$inTrain,
);

if(!$inTrain && !$inTest) {
  die "--inTrain --inTest";
}

my $oneHot = VerifyNoOverlap->new({
  inTrain => $inTrain, inTest => $inTest
});

$oneHot->go();
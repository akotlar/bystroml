use 5.10.0;
use strict;
use warnings;
use lib './lib';

use Getopt::Long;
use RefSeqXdbnsfp;

my ($in, $out, $file);

GetOptions(
  "in=s" => \$in,
  "out=s" => \$out,
  "geneFile=s" => \$file,
);

if(!$in && !$out) {
  die "--in --out --geneFile";
}

my $build = RefSeqXdbnsfp->new({
  in => $in, out => $out, geneFile => $file || './dbNSFP3.5_gene.complete.gz',
});

$build->go();
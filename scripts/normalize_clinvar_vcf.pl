use 5.10.0;
use strict;
use warnings;
use DDP;

if(@ARGV < 2) {
  die "clinvarFile1.gz clinvarFile2.gz outFile";
}

my $file1 = $ARGV[0];
my $file2 = $ARGV[1];

my $fileTemp = 'files_joined.tsv';
#First join the two clinvar files
# \kind of cool usage
# https://unix.stackexchange.com/questions/264980/how-to-append-a-line-in-a-zipped-file-without-unzipping
# my $command = "cat $file1 <(pigz -d -c $file2 | grep -v '\#' | pigz -c -) > $file";

# system("bash", "-c", $command);

system("pigz -d -c $file1 > $fileTemp");
system("pigz -d -c $file2 | grep -v '\#' >> $fileTemp");
system("pigz $fileTemp");
system("sync");

#say STDERR "WROTE $file, combining $file1 and $file2, using command $command";

open(my $fh, '-|', "pigz -d -c $fileTemp");

my $outFh;
if($ARGV[2]) {
  if($ARGV[2] =~ /.gz$/) {
    open($outFh, '|-', "pigz -c > $ARGV[2]")
  } else {
    open($outFh, '>', $ARGV[2])
  }
} else {
  $outFh = *STDOUT;
}

while(<$fh>) {
  chomp;

  if($_ =~ /^\#/) {
    say $outFh $_;
    next;
  }

  my @fields = split '\t', $_;

  # INFO field is last
  my @matches = $fields[7] =~ m/PATHOGENIC=(\d+)/g;

  my $pathogenic = 0;
  for my $match (@matches) {
    $pathogenic += $match;
  }

  $fields[7] = $pathogenic ? "pathogenic" : "notPathogenic";

  say $outFh join("\t", @fields);
}

system("rm $fileTemp.gz;");
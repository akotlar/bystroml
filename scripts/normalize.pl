use 5.10.0;
use strict;
use warnings;
use lib './lib';

use Getopt::Long;
use NormalizeOneHot;

my ($in, $out, $rank);

GetOptions(
  "in=s" => \$in,
  "out=s" => \$out,
  "ordinal" => \$rank,
);

if(!$in && !$out) {
  die "--in --out --<rank>";
}

my $oneHot = NormalizeOneHot->new({
  in => $in, out => $out
});

$oneHot->go();
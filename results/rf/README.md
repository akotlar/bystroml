## Best results obtained via
```python
clf = RandomForestClassifier()

param_dist = {
    # Relates to overfitting (more features, more overfit)
          "max_features": ["sqrt", "log2"],
    # Relates to overfitting (smaller, more likely to be deep tree, overfit)
          "min_samples_leaf": sp_randint(1, 11),
    # Reduces likelihood of overfitting if large
          "n_estimators": sp_randint(10, 500),
          "n_jobs": [8]
         }

# run randomized search
n_iter_search = 10
clf = RandomizedSearchCV(clf, param_distributions=param_dist, n_iter=n_iter_search, scoring="f1", cv=5, verbose=1)

clf.fit(cDataTrain, trainLabels)
    
print("RandomSearchCv best params", clf.best_params_)
```

## Results
```sh
[5 rows x 46 columns]
Fitting 5 folds for each of 10 candidates, totalling 50 fits
[Parallel(n_jobs=1)]: Done  50 out of  50 | elapsed: 104.4min finished
('RandomSearchCv best params', {'max_features': 'sqrt', 'n_estimators': 201, 'n_jobs': 8, 'min_samples_leaf': 9})
[0 0 0 ..., 0 0 0]
(0.84436329026184298, 0.8034671838237466)
('Confusion matrix: ', array([[264307,  15044],
       [ 19964,  81617]]))
ROC AUC: 0.96
```
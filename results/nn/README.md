The best NN model, with 2 layers of 20 nodes took too long to repeat in the iPython notebook.

In this folder are screenshots of the first 5 iteration, 3-fold CV randomized search, as well as the test results from the best model that emerged from that randomized search (solver = 'lbfgs', activation = 'logistic', hidden_layer_sizes = [20, 20])
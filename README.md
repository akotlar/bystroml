# Issues

1. xgboost install may fail due to lack of correct gcc [Help](https://pypi.org/project/xgboost/)

2) May receive a numpy warning which [should be benign](https://stackoverflow.com/questions/40845304/runtimewarning-numpy-dtype-size-changed-may-indicate-binary-incompatibility)

   ```python
   RuntimeWarning: numpy.dtype size changed, may indicate binary incompatibility. Expected 96, got 88
   ```
